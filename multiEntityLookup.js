import { LightningElement, api, track } from 'lwc';

export default class MultiEntityLookup extends LightningElement {

    _defaultEntityConfig = {
        'objName': 'All',
        'iconName': '',
        'label': 'All',
        'displayFields': 'Name',
        'displayFormat': 'Name',
        'pluralLabel': ''
    };

    @track selectedEntity;
    @track entityOptions;
    @track entityConfig;

    @api readOnly = false;
    @api multiEntity = false;

    @api recordId;
    @api entities;

    get showCombobox() {
        return this.multiEntity && !this.recordId;
    }

    connectedCallback() {

        let options = [];

        options.push({ label: 'All', value: 'All' });

        this.selectedEntity = 'All';
        this._defaultEntityConfig.searchEntities = this.entities;
        this.entityConfig = this._defaultEntityConfig;

        this.entities.forEach(entity => {
            options.push({ label: entity.label, value: entity.objName, iconName: entity.iconName });
        });

        this.entityOptions = options;
    }

    selectEntity() {

        if (this.selectedEntity === 'All') {
            this.entityConfig = this._defaultEntityConfig;
        }
        else {

            let selected = this.entities.filter(
                (entity) => entity.objName === this.selectedEntity
            );

            this.entityConfig = selected[0];
        }
    }

    handleEntityChange(event) {
        this.selectedEntity = event.detail.value;
        this.selectEntity();
    }

    handleChange(event) {
        this.recordId = event.detail.recordId;
        this.selectedEntity = event.detail.selectedEntity;
        const valueSelectedEvent = new CustomEvent('lookupselected', {detail:  this.recordId });
        this.dispatchEvent(valueSelectedEvent);
        this.selectEntity();
    }
}