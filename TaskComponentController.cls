public with sharing class TaskComponentController {
    //get total record count
    @AuraEnabled(Cacheable = true)
    public static Integer getRecordsCount(String objectName, String searchKey) { 
        String query = 'select  count() from '+objectName;
        if(!Test.isRunningTest()){
            if(searchKey != ''){
                query += ' where Subject Like  \'%' + searchKey + '%\' ';
            }
        }
        system.debug('query count :' + query);
        return Database.countQuery(query);  
    } 
    //Get Task Searching Records
    @AuraEnabled(Cacheable=true)
    public static List<Object> searchRecords(Integer pagenumber, Integer numberOfRecords, Integer pageSize, String searchKey, String objectName) {
        String query = 'select  Id, WhoId,Who.Name, WhatId,What.Name, Subject, Status, Description,OwnerId,Owner.Name from '+objectName;  
        if(!Test.isRunningTest()){
            if(searchKey != ''){
                query += ' where Subject Like  \'%' + searchKey + '%\' ';
            }
            query += ' ORDER BY Id DESC limit ' + pageSize + ' offset ' + (pageSize * (pagenumber - 1)); 
        }
        list<Object> lstRecord = Database.query(query);
        if(lstRecord.isEmpty()) {
            throw new AuraHandledException('No Record Found..');
        }
        return lstRecord;
    }
    //Update Records
    @AuraEnabled
    public static Void saveRecords(String statusValue, List<Map<String, String>> recordList) {
        String msg = '';
        try{
            Set<Id> ordids = New Set<Id>();
            List<Task> taskUpdateRecordList = new List<Task>();
            if(recordList.size() > 0){
                for(Object tasklist : recordList){
                    Map<String,Object> data = (Map<String,Object>)tasklist;
                    Task taskRec = new Task();
                    taskRec.Id = (Id)data.get('id');
                    taskRec.Status = (String)statusValue;
                    taskUpdateRecordList.add(taskRec);
                }
            }
            if(taskUpdateRecordList!=null && taskUpdateRecordList.size()>0){
                Update taskUpdateRecordList;
            }
        }
        catch(DmlException e){
            //Get All DML Messages
            for (Integer i = 0; i < e.getNumDml(); i++) {
                //Get Validation Rule & Trigger Error Messages
                msg =+ e.getDmlMessage(i) +  '\n' ;
            }
            //throw DML exception message
            throw new AuraHandledException(msg);
        }
        catch(Exception  e){
            //throw all other exception message
            throw new AuraHandledException(e.getMessage());
        }
    }
    //Get Related Contact Records
    @AuraEnabled(Cacheable=true)
    public static List<Contact> getContactRecords(String TaskId) {
        Task task1=[select id,WhatId from Task Where Id=:TaskId];
        List<Contact> conList2=new List<Contact>();
        if(task1.WhatId!=null){
            id relatedToId=task1.WhatId;
            String s1=(string.valueof(task1.WhatId));
            
            if(s1.startsWith('a02')){
                Opportunity  opp=[Select id,Account.Name from Opportunity  where id=:relatedToId];
                List<Contact> conList=[Select Id,Name,Phone,AccountId from Contact Where AccountId=:opp.Account.Name];
                conList2=conList;
            }else if(s1.startsWith('001')){
                List<Contact> conList=[Select Id,Name,Phone,AccountId from Contact Where AccountId=:relatedToId];
                conList2=conList;
            }else{
                system.debug('hello');
            }
        }
        if(conList2.isEmpty()) {
            throw new AuraHandledException('No Record Found..');
        }
        system.debug('data : ' + conList2.size());
        return conList2;
    }
    //Get Lookup Records
    @AuraEnabled(cacheable=true)
    public static List<sObject> findLookupRecords(String searchTerm, string myObject, String filter) {
        String myQuery = null;
        if(filter != null && filter != ''){
            myQuery = 'Select Id, Name from '+myObject+' Where Name Like  \'%' + searchTerm + '%\' AND '+filter+' LIMIT  5';
        }
        else {
            if(searchTerm == null || searchTerm == ''){
                myQuery = 'Select Id, Name from '+myObject+' Where LastViewedDate != NULL ORDER BY LastViewedDate DESC LIMIT  5';
            }
            else {
                myQuery = 'Select Id, Name from '+myObject+' Where Name Like  \'%' + searchTerm + '%\' LIMIT  5';
            }
        }
        List<sObject> lookUpList = database.query(myQuery);
        return lookUpList;
    }
    //Get Record By Id
    @AuraEnabled(cacheable=true)
    public static SObject lookUpById(String recordId, string objName, String fields) {
        Integer limitNum = 20;
        String finalQuery = 'SELECT ' + fields + ' FROM ' + objName;
        finalQuery += ' WHERE Id =  \'' + recordId + '\'';
        List<SObject> lookUpList = database.query(finalQuery);
        return lookUpList[0];  
    }
    //Get Record By Id
    @AuraEnabled(cacheable=true)
    public static List<List<SObject>> findRelatedToLookup(String searchTerm, string filters) {
        Integer limitNum = 10;
        String query = 'FIND \'' + searchTerm + '*\' RETURNING ' + filters + ' LIMIT ' + limitNum;
        List<List <SObject>> searchList = search.query(query);
        return searchList;
    }
    @AuraEnabled
    public static Void saveTaskRecords(List<Map<String, String>> storeCreatedRecords) {
        String msg = '';
        try{
            Set<Id> ordids = New Set<Id>();
            List<Task> taskToCreate = new List<Task>();
            if(storeCreatedRecords.size() > 0){
                for(Object task : storeCreatedRecords){
                    Map<String,Object> data = (Map<String,Object>)task;
                    Task taskRec = new Task();
                    if(data.get('assignedTo') != null && data.get('assignedTo') != ''){
                        taskRec.OwnerId = (String)data.get('assignedTo');
                    }else{
                        taskRec.OwnerId = '';
                    }
                    if(data.get('subject') != null  && data.get('subject') != ''){
                        taskRec.Subject = (String)data.get('subject');
                    }else{
                        taskRec.Subject = '';
                    }
                    if(data.get('dueDate') != null && data.get('dueDate') != ''){
                        taskRec.ActivityDate = Date.valueOf((String)data.get('dueDate'));
                    }
                    if(data.get('relatedTo') != null && data.get('relatedTo') != ''){
                        taskRec.WhatId = (Id)data.get('relatedTo');
                    }
                    if(data.get('name') != null  && data.get('name') != ''){
                        taskRec.WhoId = (Id)data.get('name');
                    }else{
                        taskRec.WhoId = '';
                    }
                    if(data.get('comments') != null  && data.get('comments') != ''){
                        taskRec.Description = (String)data.get('comments');
                    }else{
                        taskRec.Description = '';
                    }
                    if(data.get('status') != null  && data.get('status') != ''){
                        taskRec.Status = (String)data.get('status');
                    }else{
                        taskRec.Status = '';
                    }
                    if(data.get('priority') != null  && data.get('priority') != ''){
                        taskRec.Priority = (String)data.get('priority');
                    }else{
                        taskRec.Priority = '';
                    }
                    taskToCreate.add(taskRec);
                }
            }
            if(taskToCreate!=null && taskToCreate.size()>0){
                Insert taskToCreate;
            }
        }
        catch(DmlException e){
            //Get All DML Messages
            for (Integer i = 0; i < e.getNumDml(); i++) {
                //Get Validation Rule & Trigger Error Messages
                msg =+ e.getDmlMessage(i) +  '\n' ;
            }
            //throw DML exception message
            throw new AuraHandledException(msg);
        }
        catch(Exception  e){
            //throw all other exception message
            throw new AuraHandledException(e.getMessage());
        }
    }
}