import { NavigationMixin } from 'lightning/navigation';
import { LightningElement, wire, api, track } from 'lwc';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getRecordsCount from '@salesforce/apex/TaskComponentController.getRecordsCount';  
import searchRecords from '@salesforce/apex/TaskComponentController.searchRecords';
import saveRecords from '@salesforce/apex/TaskComponentController.saveRecords';
import saveTaskRecords from '@salesforce/apex/TaskComponentController.saveTaskRecords';
import getContactRecords from '@salesforce/apex/TaskComponentController.getContactRecords';
import { refreshApex } from '@salesforce/apex';
// get colums name for Status
const taskstatus = [
    {
        label: 'Not Started',
        value: 'Not Started'
    }, {
        label: 'In Progress',
        value: 'In Progress'
    }, {
        label: 'Completed',
        value: 'Completed'
    }, {
        label: 'Waiting on someone else',
        value: 'Waiting on someone else'
    }, {
        label: 'Deferred',
        value: 'Deferred'
    }
];
// get entitiesVal
const entitiesVal = [
    {
        label: 'Account',
        objName: 'Account',
        pluralLabel: 'Accounts',
        value: 'Account',
        displayFields: 'Name',
        displayFormat: 'Name',
        iconName: 'standard:account'
    }, {
        label: 'Lead',
        objName: 'Lead__c',
        pluralLabel: 'Leads',
        value: 'Lead',
        displayFields: 'First_Name__c',
        displayFormat: 'First_Name__c',
        iconName: 'standard:lead'
    }
];
// get colums name for Priority
const taskPriority = [
    {
        label: 'High',
        value: 'High'
    }, {
        label: 'Normal',
        value: 'Normal'
    }, {
        label: 'Low',
        value: 'Low'
    }
];
export default class TaskRecords extends NavigationMixin(LightningElement) {
    searchTerm = '';
    @track objectname = 'Task';
    @track recordList;
    @track noRecordsFound = true;
    @track isOpenModal = false;
    @track isCreateModalOpen = false;
    @track statusValue;
    @track contactList;
    @track error = '';
    @track entities =  entitiesVal;
    @track options =  taskstatus;
    @track optionsPriority =  taskPriority;
    @track showUpdateSection = false;
    @track selectAllCheckbox = false;
    @track selectedRecords = []; // store record list
    @track storeCreatedRecords =  [
        {           
            objName: "Task",           
            assignedTo: "",
            subject: "",
            dueDate: "",
            relatedTo: "",
            name: "",
            comments: "",
            status: "",
            priority: ""
        }
    ]; // store created list
    @track totalSelectedRecords = 0; 
    @track cssDisplay = false;
    @api currentpage;  
    @api pagesize;
    totalpages;
    localCurrentPage = null;  
    isSearchChangeExecuted = false;
	// get all record 
    renderedCallback() {
        // This line added to avoid duplicate/multiple executions of this code.  
        if (this.isSearchChangeExecuted && (this.localCurrentPage === this.currentpage)) {  
          return;  
        }  
        this.isSearchChangeExecuted = true;  
        this.localCurrentPage = this.currentpage; 
        getRecordsCount({  objectName: this.objectname , searchKey : this.searchTerm})  
          .then(recordsCount => {  
            this.cssDisplay = true;
            this.totalrecords = recordsCount;
            if (recordsCount !== 0 && !isNaN(recordsCount)) {  
                this.totalpages = Math.ceil(recordsCount / this.pagesize);
                // get reocord list
                searchRecords({ pagenumber: this.currentpage, numberOfRecords: recordsCount, pageSize: this.pagesize, searchKey: this.searchTerm, objectName: this.objectname})  
                  .then(recordList => {
                      this.cssDisplay = false;
                      this.noRecordsFound = false;
                      this.recordList = recordList; 
                      this.selectAllCheckbox = false;
                  })  
                  .catch(error => { 
                      this.cssDisplay = false; 
                      this.recordList = null; 
                  });  
              } else { 
                  this.cssDisplay = false; 
                  this.recordList = [];  
                  this.totalpages = 1;  
                  this.totalrecords = 0; 
              }  
              const event = new CustomEvent('recordsload', {  
                detail: recordsCount  
              });  
              this.dispatchEvent(event);  
          })  
          .catch(error => { 
            this.cssDisplay = false; 
            this.totalrecords = undefined;
            this.recordList = [];
          });  
      }  
	handleSearchTermChange(event) {
		this.searchTerm = event.target.value;
        this.isSearchChangeExecuted = false;  
        this.currentpage = 1;
	}
	
    handleChange(event) {
        this.statusValue = event.target.value;
    }
    allSelected(event) {
        if(event.target.checked == true){
            this.selectAllCheckbox = true;
            this.showUpdateSection = true;
            let selectedRows = this.template.querySelectorAll('lightning-input');
            // based on selected row getting values of the object
            for(let i = 1; i < selectedRows.length; i++) {
                if(selectedRows[i].type === 'checkbox') {
                    selectedRows[i].checked = event.target.checked;
                    //store record list code
                    this.selectedRecords.push({
                        id: selectedRows[i].dataset.id,
                    })
                }
            }
            this.selectedRecords = this.uniqByKeepLast(this.selectedRecords, it => it.id);
            this.totalSelectedRecords = this.selectedRecords.length;
            console.log('Select all :' + JSON.stringify(this.selectedRecords));
        }else{
            this.selectAllCheckbox = false;
            this.showUpdateSection = false;
            this.selectedRecords = [];
            this.totalSelectedRecords = 0;
            let selectedRows = this.template.querySelectorAll('lightning-input');
            for(let i = 1; i < selectedRows.length; i++) {
                if(selectedRows[i].type === 'checkbox') {
                    selectedRows[i].checked = event.target.checked;
                }
            }
        }
    }
    uniqByKeepLast(data, key) {
        return [
            ...new Map(
                data.map(x => [key(x), x])
            ).values()
        ]
    }
    handleCheckbox(event){
        this.isChecked = event.target.checked;
        if(this.isChecked == true){
            this.showUpdateSection = true;
            //store line item list code
            this.selectedRecords.push({
                id: event.target.dataset.id
            });
        }else{
            this.showUpdateSection = false;
            //get line item list by id
            let element = this.selectedRecords.find(ele  => ele.id === event.target.dataset.id);
            this.selectedRecords.splice(this.selectedRecords.indexOf(element),1);
        }
        //store record list
        this.selectedRecords = this.uniqByKeepLast(this.selectedRecords, it => it.id);
        this.totalSelectedRecords = this.selectedRecords.length;
        console.log(JSON.stringify(this.selectedRecords));
    }

    handleOpenModal(event) {
        this.isOpenModal = true;
        this.cssDisplay = true;
        getContactRecords({
            TaskId: event.target.dataset.id
        })
        .then(recordList => {
            this.cssDisplay = false;
            this.contactList = recordList;
            this.error = '';
        })
        .catch(error => {
            this.cssDisplay = false;
            this.error = error.body.message;
            this.contactList = null;
        });
    }
   
    handleCloseModal() {
        this.isOpenModal = false;
        this.cssDisplay = false;
    }

    openCreateModal() {
        this.isCreateModalOpen = true;
    }
    closeCreateModal() {
        // to close modal set isModalOpen tarck value as false
        this.isCreateModalOpen = false;
    }

    genericOnChange(event){
        let element = this.storeCreatedRecords.find(ele  => ele.objName === 'Task');
        if(element != 'undefined'){
            if(event.target.name == 'subject'){
                element.subject = event.target.value;
            }else if(event.target.name == 'duedate'){
                element.dueDate = event.target.value;
            }else if(event.target.name =='commentval'){
                element.comments = event.target.value;
            }
        }
        this.storeCreatedRecords = [...this.storeCreatedRecords];
        console.log(JSON.stringify(this.storeCreatedRecords));
    }

    handleObjSelection(event){
        let element = this.storeCreatedRecords.find(ele  => ele.objName === 'Task');
        if(element != 'undefined'){
            element.relatedTo = event.detail;
        }
        this.storeCreatedRecords = [...this.storeCreatedRecords];
        console.log(JSON.stringify(this.storeCreatedRecords));
    }

    handleUserSelection(event){
        let element = this.storeCreatedRecords.find(ele  => ele.objName === 'Task');
        if(element != 'undefined'){
            element.assignedTo = event.detail;
        }
        this.storeCreatedRecords = [...this.storeCreatedRecords];
        console.log(JSON.stringify(this.storeCreatedRecords));
    }

    handleContactSelection(event){
        let element = this.storeCreatedRecords.find(ele  => ele.objName === 'Task');
        if(element != 'undefined'){
            element.name = event.detail;
        }
        this.storeCreatedRecords = [...this.storeCreatedRecords];
        console.log(JSON.stringify(this.storeCreatedRecords));
    }

    handleStatusChange(event) {
        let element = this.storeCreatedRecords.find(ele  => ele.objName === 'Task');
        if(element != 'undefined'){
            element.status = event.target.value;
        }
        this.storeCreatedRecords = [...this.storeCreatedRecords];
        console.log(JSON.stringify(this.storeCreatedRecords));
    }

    handlePriorityChange(event) {
        let element = this.storeCreatedRecords.find(ele  => ele.objName === 'Task');
        if(element != 'undefined'){
            element.priority = event.target.value;
        }
        this.storeCreatedRecords = [...this.storeCreatedRecords];
        console.log(JSON.stringify(this.storeCreatedRecords));
    }

    handleSaveClick(){
        let errorCount = 0;
        let fixedCount = this.selectedRecords.length;
        if(fixedCount > 0){
            if(this.statusValue == "" || this.statusValue == null){
                errorCount = 1;
            }
        }
        if(errorCount == 0){
            this.cssDisplay = true;
            saveRecords({
                statusValue: this.statusValue,
                recordList: this.selectedRecords,
            })
            .then(result => {
                this.showUpdateSection = false;
                this.dispatchToastEvent("success", "Records Update Successfully");
                this.cssDisplay = false;
                window.location.reload();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error Updating record',
                        message: error.body.message + ' \nStatus: ' + error.statusText,
                        variant: 'error'
                    })
                );
                this.cssDisplay = false;
            });
        }else{
            this.dispatchToastEvent("error", "Please select status.");
        }
    }

    submitDetails(){
        let errorCount = 0;
        let fixedCount = this.storeCreatedRecords.length;
        if(fixedCount > 0){
            for(let i = 0; i < this.storeCreatedRecords.length; i++) {
                if(this.storeCreatedRecords[i].subject == "" || this.storeCreatedRecords[i].subject == null || this.storeCreatedRecords[i].relatedTo == ''){
                    errorCount = 1;
                }
            }
        }
        if(errorCount == 0){
            this.cssDisplay = true;
            saveTaskRecords({
                storeCreatedRecords: this.storeCreatedRecords
            })
            .then(result => {
                this.isCreateModalOpen = false;
                this.dispatchToastEvent("success", "Records Save Successfully");
                this.cssDisplay = false;
                window.location.reload();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error saving record',
                        message: error.body.message + ' \nStatus: ' + error.statusText,
                        variant: 'error'
                    })
                );
                this.cssDisplay = false;
            });
        }else{
            this.dispatchToastEvent("error", "These required fields must be completed: Related To, Subject.");
        }
    }
    dispatchToastEvent(type, msg) {
        if (type === "success") {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Success",
              message: msg,
              variant: "success"
            })
          );
        } else {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Error",
              message: msg,
              variant: "error"
            })
          );
        }
    }

}